/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upis.sisim.pojo;

import br.upis.sisim.dao.PessoaNivel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Francisco Ernesto Teixeira <fco.ernesto@gmail.com>
 */
public class Pessoa {

    private boolean autorizado;
    private String usuario;
    private String nome;
    private PessoaNivel nivel;
    private String acesso;
    private List<PessoaArea> area;

    public Pessoa() {
        this.autorizado = false;
        this.usuario = null;
        this.nome = null;
        this.nivel = null;
        this.acesso = null;
        this.area = null;
    }

    public boolean isAutorizado() {
        return autorizado;
    }

    public void setAutorizado(boolean autorizado) {
        this.autorizado = autorizado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public PessoaNivel getNivel() {
        return nivel;
    }

    public void setNivel(PessoaNivel nivel) {
        this.nivel = nivel;
    }

    public String getAcesso() {
        return acesso;
    }

    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

    public List<PessoaArea> getArea() {
        if (area == null) {
            area = new ArrayList<PessoaArea>();
        }

        return area;
    }

    public void setArea(List<PessoaArea> area) {
        this.area = area;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upis.sisim.pojo;

/**
 *
 * @author Francisco Ernesto Teixeira <fco.ernesto@gmail.com>
 */
public class PessoaArea {

    private Long id;
    private String nome;
    
    public PessoaArea() {
        this.id = null;
        this.nome = null;
    }

    public PessoaArea(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

<style>
    #assuntos-dropDownButton {
        margin-bottom: .5em;
    }

    #assuntos-Tree ul {
        margin-left: 0;
    }

    #assuntos-Tree sup, #dropDownButtonContentassuntos-dropDownButton div sup {
        font-size: 0.6em;
        color: #888;
    }

    #assuntos-Tree .jqx-fill-state-hover sup, #assuntos-Tree .jqx-fill-state-pressed sup {
        color: #fff;
    }

    #questao-respostas {
        margin-bottom: 10px;
    }
</style>
<script type="text/javascript">
var endereco, salvado;

endereco = '<?php Uri::base(); ?>/sisim/rest/cadastro/questoes';
salvado = false;

/**
 * Configuração do CKEditor
 */
CKEDITOR.config.language = 'pt-br';
CKEDITOR.config.skin = 'moonocolor';
CKEDITOR.config.toolbarLocation = 'bottom';
CKEDITOR.config.toolbar = [
    ['Styles', 'Format', 'Font'],
    ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'RemoveFormat'],
    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
    ['Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste']
];
CKEDITOR.config.resize_enabled = false;

// estrutura da resposta
function QuestaoResposta() {
    this.letra = null;
    this.conteudo = null;
    this.selecionada = null;
}

// estrutua da questão
function Questao() {
    this.id = null;
    this.assunto = null;
    this.tipo = null;
    this.conteudo = null;
    this.respostas = [];
    this.explicacao = null;
}

/**
 * Inicialização do processamento.
 */
$(document).ready(function () {
    var origem, adaptadorDeDados, assuntosDropDownButton, assuntosTree, assuntosOrigem, questaoAbas;

    // popula os dados do combo Assunto
    assuntosDropDownButton = $('#assuntos-dropDownButton');
    assuntosTree = $('#assuntos-Tree');
    assuntosOrigem = null;
    questaoAbas = $('#questao-abas');

    $.ajax({
        async: false,
        dataType: 'json',
        url: endereco + '/../assuntos/disciplinas.json',
        success: function (data, status, xhr) {
            source = data;
        }
    });

    assuntosDropDownButton
        .jqxDropDownButton({
            width: 234,
            height: 25,
            theme: 'bootstrap'
        });
    assuntosTree
        .on('initialized', function (event) {
            var assuntosTree, items;

            assuntosTree = $('#assuntos-Tree');
            items = assuntosTree.jqxTree('getItems');

            $(items).each(function () {
                if (this.label.indexOf('Disciplina') > 0) {
                    assuntosTree.jqxTree('disableItem', this.element);
                }
            });
        })
        .on('select', function (event) {
            var args, item, dropDownContent;

            args = event.args;
            item = assuntosTree.jqxTree('getItem', args.element);

            dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + item.label + '</div>';
            assuntosDropDownButton.jqxDropDownButton('setContent', dropDownContent);

            assuntosDropDownButton.jqxDropDownButton('close');
        })
        .jqxTree({
            source: source,
            width: 280,
            height: 220,
            theme: 'bootstrap'
        });

    // habilita a caixa de procura
    $('#procurar').enterKey(function () {
        // atualiza a listagem
        $('#questoes').jqxListBox('refresh');
    });

    // prepara os dados
    origem =
    {
        datatype: 'json',
        datafields: [
            {
                name: 'rotulo'
            },
            {
                name: 'valor'
            }
        ],
        id: 'id',
        url: endereco + '.json'
    };
    adaptadorDeDados = new $.jqx.dataAdapter(origem, {
        loadServerData: function (serverdata, source, callback) {
            var procurarpor;

            procurarpor = $('#procurar').val().trim();

            if (procurarpor.length > 0) {
                serverdata = {
                    'procurarpor': procurarpor
                };
            }

            $.ajax({
                dataType: source.datatype,
                url: source.url,
                data: serverdata,
                success: function (data, status, xhr) {
                    var dataArray, i, record, datarow, datafield, value;

                    dataArray = new Array();

                    if (data !== undefined) {
                        for (i = 0; i < data.length; (i = i + 1)) {
                            record = data[i];
                            datarow = {};

                            datarow['rotulo'] = record.rotulo;
                            datarow['valor'] = record.valor;

                            dataArray[dataArray.length] = datarow;
                        }
                    }

                    // envia os registros carregados para o plugin jqxDataAdapter
                    callback({ records: dataArray });

                    setTimeout('selecionarAoAdicionar()', 50);
                    setTimeout('destacarProcuraNaListagem("procurar", "questoes")', 50);
                }
            });
        }
    });
    // cria um jqxListBox
    $('#questoes').jqxListBox({
        source: adaptadorDeDados,
        displayMember: 'rotulo',
        valueMember: 'valor',
        width: '100%',
        height: 303,
        theme: 'bootstrap'
    });
    $('#questoes').on('select', function (event) {
        var item, value;

        if (event.args) {
            item = event.args.item;
            if (item) {
                value = item.value;

                $('#id').val(value.id);
                /*
                 $('#tipo').val(value.Tipo);
                 $('#quantidade').val(value.Quantidade);
                 $('#conteudo').val(value.Conteudo);
                 $('#respostaCerta').val(value.RespostaCerta);

                 $('#conteudo').blur().focus();

                 $('#painel-controle .esquerda').fadeIn('fast');
                 */

                removerNotificacoesErro();
            }
        }
    });

    // prepara os campos
    $('#conteudo')
        .filter_input({
            regex: '[A-Za-zÀ-ú ]'
        });
    $('#quantidade')
        .filter_input({
            regex: '[0-9]'
        })
        .spinner({
            min: 1,
            max: 5
        });
    // DONE: Corrige a aparência dos "spinners"
    $('.ui-spinner-input').removeClass('ui-spinner-input');

    // adiciona um asterisco vermelho para os campos obrigatórios
    definirComoObrigatorio();

    // ao clicar no botão "Adicionar"
    $('#adicionar').click(function () {
        var tamanho, i, titulo;

        removerNotificacoes();

        // deseleciona algum item da listagem
        $('#questoes').jqxListBox('clearSelection');

        // limpa o formulário
        assuntosDropDownButton.jqxDropDownButton('setContent', new String());
        assuntosTree.jqxTree('selectItem', null);
        $('#tipoS').attr('checked', true);
        $('#id, #conteudo, #explicacao').val(new String());
        setTimeout('CKEDITOR.instances.conteudo.setData(new String());CKEDITOR.instances.explicacao.setData(new String());', 50);
        tamanho = questaoAbas.jqxTabs('length');
        questaoAbas.jqxTabs('enableAt', tamanho - 2);
        for (i = tamanho - 1; i > 0; (i = i - 1)) {
            titulo = questaoAbas.jqxTabs('getTitleAt', i);
            if ((titulo.indexOf('Resp.') > 0) && (titulo.indexOf('*') > 0)) {
                questaoAbas.jqxTabs('removeAt', i);
            }
        }
        setTimeout('$("#questao-abas").val(0);', 50);
    }).click();

    // ao clicar no botão "Salvar"
    $('#salvar').click(function (e) {
        var janela;

        e.preventDefault();

        // previne continuar a ação caso o botão esteja desabilitado
        if ($(this).hasClass('disabled')) {
            return false;
        }

        janela = $('<div id="#dialog-salvar" />')
            .html('Deseja salvar?')
            .dialog({
                modal: true,
                closeOnEscape: false,
                resizable: false,
                title: 'Confirmação',
                buttons: {
                    'Não': function () {
                        $(this).dialog('close');

                        return false;
                    },
                    'Sim': function () {
                        var questao, valido, haselecao;

                        questao = extrairQuestao();
                        valido = true;

                        $(this).dialog('close');

                        // valida os campos do formulário e caso seja válido, envia
                        if (questao.assunto === null) {
                            notificarErro('<i class="icon-warning-sign"></i> Assunto: Este campo é requerido.');
                            valido = false;
                        }

                        // questao.tipo já é definido

                        if (questao.conteudo.length === 0) {
                            notificarErro('<i class="icon-warning-sign"></i> Conteúdo da Questão: Este campo é requerido.');
                            valido = false;
                        }

                        if (questao.respostas < 2) {
                            notificarErro('<i class="icon-warning-sign"></i> Respostas: Devem haver no mínimo 2 respostas.');
                            valido = false;
                        } else {
                            haselecao = false;
                            for (var i = 0; i < questao.respostas.length; (i = i + 1)) {
                                if (questao.respostas[i].conteudo.length === 0) {
                                    notificarErro('<i class="icon-warning-sign"></i> Resposta ' + questao.respostas[i].letra + ': Este campo é requerido.');
                                    valido = false;
                                }

                                if (questao.respostas[i].selecionada) {
                                    haselecao = true;
                                }
                            }

                            if (!haselecao) {
                                notificarErro('<i class="icon-warning-sign"></i> Deve haver ao menos uma resposta selecionada.');
                                valido = false;
                            }
                        }

                        if (valido) {
                            // remove todas as notificações
                            $('.qtip').remove();

                            // notifica ao formulário que ele está/foi salvo
                            salvado = true;

                            // solicita a inserção/alteração do registro
                            notificarInformacao('<i class="icon-info-sign"></i> Salvando...');
                            $.ajax({
                                url: endereco,
                                data: {
                                    'questao': JSON.stringify(questao)
                                },
                                type: (questao.id.length === 0) ? 'POST' : 'PUT',
                                success: function (result) {

                                    removerNotificacoes();

                                    // se tiver sido OK
                                    if (result === 'ok') {
                                        notificarSucesso('<i class="icon-ok"></i> Salvo com sucesso!');

                                        // atualiza a lista de questoes
                                        $('#questoes').jqxListBox('refresh');
                                    }
                                    // do contrário
                                    else {
                                        notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                                    }
                                },
                                error: function (result) {
                                    notificarErro('<i class="icon-warning-sign"></i> Ocorreu um erro ao salvar.');
                                }
                            });
                        }

                        return false;
                    }
                },
                open: function () {
                    // remove o botão de fechar
                    $(this).parent().find('.ui-dialog-titlebar-close').remove();

                    $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                        $(this).addClass('btn');
                        if ($(this).text() === 'Sim') {
                            $(this).prepend('<i class="fam-tick"></i> ');
                        } else if ($(this).text() === 'Não') {
                            $(this).prepend('<i class="fam-cross"></i> ').focus();
                        }
                    });
                    //addClass('btn').focus();
                },
                close: function () {
                    // destrói a janela
                    $(janela).parent().remove();
                    $(janela).remove();
                    janela = null;
                }
            });
    });

    $('#tipoS, #tipoM').click(function () {
        gerarRotulosRespostas();
    });

    questaoAbas
        .jqxTabs({
            height: '281px',
            width: '940px',
            theme: 'bootstrap'
        })
        .on('tabclick', function (event) {
            var indice, rotulo, tamanho;

            indice = event.args.item;
            rotulo = questaoAbas.jqxTabs('getTitleAt', indice);

            // lembrando que o tamanho no máximo é: 1 questão + 5 respostas + 1 explicação +
            // 1 adicionar. resp. (caso não tenha alcançado o limite)
            tamanho = questaoAbas.jqxTabs('length');

            if ((rotulo.trim() == 'Adicionar Resp.') && (tamanho < 8)) {
                adicionarResposta();
            }

            tamanho = questaoAbas.jqxTabs('length');
            if (tamanho >= 8) {
                questaoAbas.jqxTabs('disableAt', tamanho - 2);
            }
        });

    CKEDITOR.replace('explicacao', {
        uiColor: '#fde647'
    });
});

/**
 * Gera uma cor randômica.
 *
 * @returns {string}
 */
function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

/**
 * Solicita a confirmação de remoção do registro.
 */
function remover() {
    var item, value, janela;

    item = $('#questoes').jqxListBox('getSelectedItem');
    value = item.value;

    janela = $('<div id="#dialog-remover" />')
        .html('Deseja remover o questao "' + value.id + ' - ' + value.Conteudo + '"?')
        .dialog({
            modal: true,
            closeOnEscape: false,
            resizable: false,
            title: 'Confirmação',
            buttons: {
                'Não': function () {
                    $(this).dialog('close');

                    return false;
                },
                'Sim': function () {
                    $.ajax({
                        url: endereco,
                        data: {
                            'id': value.id
                        },
                        type: 'DELETE',
                        success: function (result) {
                            var questoes;

                            // fecha a janela
                            janela.dialog('close');

                            if (result === 'ok') {
                                // remove o item selecionado
                                questoes = $('#questoes');
                                questoes.jqxListBox('removeAt', questoes.jqxListBox('getSelectedIndex'));

                                // limpa o formulário / coloca uma ficha vazia
                                $('#adicionar').click();
                            }
                        }
                    });
                }
            },
            open: function () {
                // remove o botão de fechar
                $(this).parent().find('.ui-dialog-titlebar-close').remove();

                $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                    $(this).addClass('btn');
                    if ($(this).text() === 'Sim') {
                        $(this).prepend('<i class="fam-tick"></i> ');
                    } else if ($(this).text() === 'Não') {
                        $(this).prepend('<i class="fam-cross"></i> ').focus();
                    }
                });
            },
            close: function () {
                // destrói a janela
                $(janela).parent().remove();
                $(janela).remove();
                janela = null;
            }
        });
}

function adicionarResposta() {
    var questaoAbas, i, id;

    questaoAbas = $('#questao-abas');
    i = questaoAbas.jqxTabs('length') - 2;
    id = 'resposta_' + Math.round(Math.random() * 999999);
    questaoAbas.jqxTabs('addAt', i, new String(), '<textarea class="ckeditor" id="' + id + '" name="resposta[]"></textarea>');

    // adiciona o editor WYSIWYG
    CKEDITOR.replace(id, {
        uiColor: get_random_color()
    });

    gerarRotulosRespostas();

    // adiciona o botão de remover
    setTimeout("$('#cke_" + id + " .cke_toolbox').prepend('<span class=\"cke_toolbar\" role=\"toolbar\"><span class=\"cke_toolbar_start\"></span><span class=\"cke_toolgroup\" role=\"presentation\"><a class=\"cke_button cke_button_off\" style=\"cursor:pointer;\" href=\"javascript:void(\\\'Remover Resposta\\\')\" onclick=\"removerResposta();\" title=\"Remover Resposta\"><span class=\"cke_button_icon\" style=\"background-image:url(../assets/js/vendor/ckeditor/skins/moonocolor/images/close.png);background-position:center center;\">&nbsp;</span><span class=\"cke_button_label\">Remover</span></a></span><span class=\"cke_toolbar_end\"></span></span>');", 100);
}

function removerResposta() {
    var janela, questaoAbas, tamanho, i, letra;

    questaoAbas = $('#questao-abas');
    tamanho = questaoAbas.jqxTabs('length');
    i = questaoAbas.val();
    letra = questaoAbas.jqxTabs('getTitleAt', i).trim().split(' ');
    letra = letra[1];

    janela = $('<div id="#dialog-remover" />')
        .html('Deseja remover resposta "' + letra + '"?')
        .dialog({
            modal: true,
            closeOnEscape: false,
            resizable: false,
            title: 'Confirmação',
            buttons: {
                'Não': function () {
                    $(this).dialog('close');

                    return false;
                },
                'Sim': function () {
                    questaoAbas.jqxTabs('enableAt', tamanho - 2);
                    questaoAbas.jqxTabs('removeAt', i);

                    gerarRotulosRespostas();

                    // fecha a janela
                    janela.dialog('close');
                }
            },
            open: function () {
                // remove o botão de fechar
                $(this).parent().find('.ui-dialog-titlebar-close').remove();

                $('.ui-dialog-buttonpane button', $(this).parent()).each(function () {
                    $(this).addClass('btn');
                    if ($(this).text() === 'Sim') {
                        $(this).prepend('<i class="fam-tick"></i> ');
                    } else if ($(this).text() === 'Não') {
                        $(this).prepend('<i class="fam-cross"></i> ').focus();
                    }
                });
            },
            close: function () {
                // destrói a janela
                $(janela).parent().remove();
                $(janela).remove();
                janela = null;
            }
        });
}

function obterTipo() {
    return $('input[name=tipo]:checked').val();
}

function obterSelecao(letra, selecionado) {
    return '<input id="resposta_' + letra + '" name="resposta" type="' + ((obterTipo() === 'S') ? 'radio' : 'checkbox') +
        (selecionado ? 'checked="true"' : '') + '" value="' + letra + '" />';
}

function gerarRotulosRespostas() {
    var questaoAbas, tamanho, i, letra;

    questaoAbas = $('#questao-abas');
    tamanho = questaoAbas.jqxTabs('length');
    if (tamanho > 3) {
        i = 0;
        questaoAbas.find('.jqx-tabs-title').each(function () {
            if ((i > 0) && (i < (tamanho - 2))) {
                letra = String.fromCharCode(i + 64);
                $(this).html(obterSelecao(letra, false) + ' Resp. ' + letra + ' <span class="obrigatorio"></span>');
            }

            i++;
        });

        definirComoObrigatorio();
    }
}

function selecionarAoAdicionar() {
    var id, questoes, total;

    // se veio de um processamento de salvar
    if (salvado) {
        id = $('#id').val();
        questoes = $('#questoes');
        total = questoes.jqxListBox('getItems').length;

        // se já existem valores e a identificação está vazia
        if ((total > 0) && (id <= 0)) {
            // seleciona o último item
            questoes.jqxListBox('selectIndex', (total - 1));
        }
        // não existem itens selecionados
        else if (questoes.jqxListBox('getSelectedIndex') < 0) {
            setTimeout('selecionarAoAdicionar()', 50);
        }
    }
}

/**
 * Extrai a questão do formulário.
 *
 * @returns {Questao}
 */
function extrairQuestao() {
    var assuntosTree, questaoAbas, questao, item, resposta, p, tempresposta, temp;

    assuntosTree = $('#assuntos-Tree');
    questaoAbas = $('#questao-abas');

    questao = new Questao();
    questao.id = $('#id').val();
    item = assuntosTree.jqxTree('getSelectedItem');
    if (item !== null) {
        questao.assunto = item.value;
    }
    questao.conteudo = CKEDITOR.instances.conteudo.getData();
    questao.tipo = ($('#tipoS').attr('checked')) ? 'S' : 'M';
    questao.explicacao = CKEDITOR.instances.explicacao.getData();

    tempresposta = '';
    $('input[name=resposta]:checked').each(function () {
        tempresposta += $(this).val();
    });

    for (var i in CKEDITOR.instances) {
        if (CKEDITOR.instances[i].name.indexOf('resposta') > -1) {
            resposta = new QuestaoResposta();

            temp = questaoAbas.find('div.jqx-tabs-content-element');
            for (p = 0; p < temp.length; (p = p + 1)) {
                if ($(temp[p]).find('textarea[id=' + CKEDITOR.instances[i].name + ']').length > 0) {
                    resposta.conteudo = CKEDITOR.instances[i].getData();
                    resposta.letra = String.fromCharCode((p - 2) + 64);
                    resposta.selecionada = (tempresposta.indexOf(resposta.letra) > -1);

                    break;
                }
            }

            questao.respostas.push(resposta);
        }
    }

    return questao;
}
</script>
<div>
    <div class="coluna esquerda">
        <input id="procurar" type="text" placeholder="Digite e aperte ENTER para procurar"/>

        <button id="adicionar" class="btn"><i class="fam-add"></i> Adicionar</button>
        <label>Questão</label>

        <div id="questoes" class="clear"></div>
    </div>
    <div class="coluna conteudo">
        <form id="questao">
            <fieldset>
                <legend>Propriedades</legend>
                <p>Nesta página você pode definir as propriedades da questão. Selecione a(s) resposta(s) de acordo com o
                    tipo.</p>

                <!-- Identificação -->
                <input id="id" type="hidden"/>

                <!-- Assunto -->
                <div class="campo two-width">
                    <label class="obrigatorio">Assunto</label>

                    <div id="assuntos-dropDownButton">
                        <div style="border: none;" id='assuntos-Tree'></div>
                    </div>
                </div>

                <!-- Tipo -->
                <div class="campo two-width">
                    <label class="obrigatorio">Tipo</label><br/>
                    <input id="tipoS" name="tipo" data-titulo="Tipo" type="radio" value="S"/> <label for="tipoS">Seleção
                        Simples</label>
                    <a href="#" class="tip" data-toggle="tooltip" data-placement="bottom"
                       title="Utilizada para questões que possuem apenas uma resposta correta."><i
                            class="fam-help"></i></a>
                    <input id="tipoM" name="tipo" data-titulo="Tipo" type="radio" value="M"/> <label for="tipoM">Múltipla
                        Seleção</label>
                    <a href="#" class="tip" data-toggle="tooltip" data-placement="bottom"
                       title="Utilizada para questões que possuem mais de uma resposta correta."><i
                            class="fam-help"></i></a>
                </div>

                <br class="clear"/>

                <div id='questao-abas'>
                    <ul>
                        <li><i class="fam-help"></i> Questão <span class="obrigatorio"></span></li>
                        <li><i class="fam-add"></i> Adicionar Resp.</li>
                        <li><i class="fam-lightbulb"></i> Explicação</li>
                    </ul>
                    <div>
                        <textarea class="ckeditor" id="conteudo" name="conteudo" data-titulo="Questão"></textarea>
                    </div>
                    <div></div>
                    <div>
                        <textarea class="ckeditor" id="explicacao" name="explicacao"></textarea>
                    </div>
                </div>

                <br class="clear"/>

                <!-- Painel de Controle -->
                <div id="painel-controle">
                    <div class="direita">
                        <button id="salvar" class="btn"><i class="fam-disk"></i> Salvar</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<br class="clear"/>
<script type="text/javascript">
    var endereco;

    endereco = '<?php Uri::base(); ?>/sisim/rest/cadastro/turmas/relatorio/simulado/';

    /**
     * Inicialização do processamento.
     */
    $(document).ready(function () {
        definirComoObrigatorio();

        $('#form-simulado').validate({
            rules: {
                'simulado': 'required'
            },
            onfocusout: function (element) {
                this.element(element);
            },
            onkeyup: false,
            errorPlacement: function (error, element) {
                var existe, mensagem, mensagens, i;

                existe = false;
                mensagem = '<i class="icon-warning-sign"></i> ' + element.data('titulo') + ': ' + error.text();

                // verifica se já existe uma notificação com o título
                mensagens = $('.bootstrap-growl').toArray();
                for (i = 0; i < mensagens.length; (i = i + 1)) {
                    if ($(mensagens[i]).html() === mensagem) {
                        existe = true;
                        break;
                    }
                }

                if (!existe) {
                    notificarErro(mensagem);
                }
            },
            submitHandler: function (form) {
                window.location.href = '<?php echo Uri::base(); ?>relatorio/turmas/' + $('#simulado option:selected').val();
            }
        });

        <?php if (isset($resumo)) { ?>
        $('#relatorio').dataTable({
            'sDom': "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            'sPaginationType': 'bootstrap',
            'oLanguage': {
                'sLengthMenu': '_MENU_ records per page'
            },
            'bPaginate': false,
            'bInfo': false,
            'bFilter': false,
            'bProcessing': true,
            'sAjaxSource': endereco + '<?php echo $selecionado; ?>',
            'aoColumns': [
                { 'mData': 'Nome' },
                { 'mData': 'TempoMedio' },
                { 'mData': 'DesempenhoMedio' },
                { 'mData': 'id' }
            ],
            'aoColumnDefs': [
                {
                    'sClass': 'right column',
                    'aTargets': [1, 2]
                },
                {
                    // `data` refers to the data for the cell (defined by `mData`, which
                    // defaults to the column being worked with, in this case is the first
                    // Using `row[0]` is equivalent.
                    'mRender': function (data, type, row) {
                        return '<a href="<?php echo Uri::base(); ?>relatorio/turmas/' + data + '">Detalhes</a>';
                    },
                    'aTargets': [3]
                }
            ]
        });
        <?php } ?>
    });
</script>
<form id="form-simulado" class="form-inline">
    <?php if (isset($resumo)) { ?>
        <div style="float:right">
            <a href="#">Ver questões base do simulado</a>
        </div>
    <?php } ?>
    <label class="obrigatorio">Simulado</label>
    <select id="simulado" name="simulado" data-titulo="Simulado">
        <option value=""></option>
        <?php
        foreach ($simulados as $simulado) {
            echo '<option value="' . $simulado['id'] . '"' . (($simulado['id'] === $selecionado) ? ' selected="true"' : '') . '>' . $simulado['Titulo'] . '</option>';
        }
        ?>
    </select>
    <button type="submit" class="btn">Selecionar</button>
</form>
<?php if (isset($resumo)) { ?>
    <!-- quadro de resumo -->
    <div class="alert alert-info alert-fix">
        <div class="alert-col">
            <div class="medium-width">Início em <?php echo $resumo['Inicio']; ?></div>
            <div class="medium-width">Quantidade de questões: <?php echo $resumo['Questoes']; ?></div>
        </div>
        <div class="alert-col">
            <div class="medium-width">Tempo máximo de <?php echo $resumo['Duracao']; ?> minutos</div>            
            <div class="medium-width">Pontuação mínima: <?php echo $resumo['Pontuacao']; ?>%</div>
        </div>
    </div>

    <!-- turmas -->
    <table id="relatorio" class="table table-striped table-bordered" cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Turma</th>
            <th>Tempo Médio (min)</th>
            <th>Desempenho Médio (%)</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        <tr>
            <th>Média Total</th>
            <th><?php echo $resumo['TempoMedio']; ?></th>
            <th><?php echo $resumo['DesempenhoMedio']; ?></th>
            <th></th>
        </tr>
        </tfoot>
    </table>
<?php } ?>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: francisco
 * Date: 5/31/13
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<script type="text/javascript">
    var endereco;

    endereco = '<?php Uri::base(); ?>/sisim/rest/cadastro/simulados/relatorio.json';

    /**
     * Inicialização do processamento.
     */
    $(document).ready(function () {
        $('#relatorio').dataTable({
            'sDom': "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            'sPaginationType': 'bootstrap',
            'oLanguage': {
                'sLengthMenu': '_MENU_ records per page'
            },
            'bPaginate': false,
            'bInfo': false,
            'bFilter': false,
            'bProcessing': true,
            'sAjaxSource': endereco,
            'aoColumns': [
                { 'mData': 'Titulo' },
                { 'mData': 'TempoMedio' },
                { 'mData': 'DesempenhoMedio' },
                { 'mData': 'id' }
            ],
            'aoColumnDefs': [
                {
                    'sClass': 'right column',
                    'aTargets': [1, 2]
                },
                {
                    // `data` refers to the data for the cell (defined by `mData`, which
                    // defaults to the column being worked with, in this case is the first
                    // Using `row[0]` is equivalent.
                    'mRender': function (data, type, row) {
                        return '<a href="<?php echo Uri::base(); ?>relatorio/turmas/' + data + '">Detalhes</a>';
                    },
                    'aTargets': [3]
                }
            ]
        });
    });
</script>
<table id="relatorio" class="table table-striped table-bordered" cellpadding="0" cellspacing="0" border="0">
    <thead>
    <tr>
        <th>Simulado</th>
        <th>Tempo Médio (min)</th>
        <th>Desempenho Médio (%)</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <th>Média Total</th>
        <th><?php echo $mediaTotal['TempoMedio']; ?></th>
        <th><?php echo $mediaTotal['DesempenhoMedio']; ?></th>
        <th></th>
    </tr>
    </tfoot>
</table>
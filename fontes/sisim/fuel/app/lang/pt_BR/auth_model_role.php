<?php

return array(
	'name'          => 'Nome da regra',
	'filter'        => 'Permissões especiais',

	'permissions'   => array(
		''          => 'Nenhum',
		'A'         => 'Permitir todos os acessos',
		'D'         => 'Negar todos os acessos',
		'R'         => 'Revogar todas as permissões atribuídas',
	),
);

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jadypamella
 * Date: 07/05/13
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome 404 view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Relatorio_Turmas_Index extends ViewModel
{
    /**
     * Prepare the view data, keeping this in here helps clean up
     * the controller.
     *
     * @return void
     */
    public function view()
    {
        //$this->pessoas = $this->request()->param('name', 'World');
    }
}

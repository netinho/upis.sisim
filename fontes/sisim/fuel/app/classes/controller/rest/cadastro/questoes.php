<?php

/**
 * Class Controller_Rest_Cadastro_Questoes
 *
 * Classe que cuida do CRUD do cadastro das questões.
 */
class Controller_Rest_Cadastro_Questoes extends Controller_Rest
{

    private $usuario;

    /**
     * Construtor da classe.
     */
    function __construct(\Request $request) // nesse caso o construtor recebe a requisição tal qual o construtor de seu pai
    {
        // chama o construtor da classe pai
        parent::__construct($request);

        // obtém o usuário da sessão
        $usuario = \Session::get('usuario');

        // obtém a pessoa
        $pessoas = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        foreach ($pessoas as $value) {
            $this->usuario = $value;
        }
    }

    /**
     * Obtém todos os registros para a listagem do seu cadastro.
     * @return array
     */
    public function get_index()
    {
        // obtém o parâmetro de pesquisa
        $procurarpor = Input::get('procurarpor');

        // caso não esteja vazio, deve procurar por
        if (!empty($procurarpor)) {
            // procura todos os registros com o seguinte título e em ordem crescente
            $registros = Model_Questao::find('all', array(
                'where' => array(
                    array('conteudo', 'LIKE', '%' . $procurarpor . '%'),
                ),
                'order_by' => array('conteudo' => 'asc'),
            ));
        } // do contrário
        else {
            // obtém todos os registros
            $registros = Model_Questao::find('all', array(
                    'order_by' => array('conteudo' => 'asc'))
            );
        }

        // processamento dos registros, pois eles precisam ser formatados para aproveitar a estrutura do jqxListBox
        $registros = $this->processarRegistros($registros);

        // retorna os registros encontrados e processados
        return $registros;
    }

    /**
     * Insere um novo registro.
     * @return array
     */
    public function post_index()
    {
        $_questao = json_decode(Input::post('questao'));

        $questao = Model_Questao::forge();
        $questao->Assunto = $_questao->assunto;
        $questao->Tipo = $_questao->tipo;
        $questao->Conteudo = $_questao->conteudo;
        $questao->Explicacao = $_questao->explicacao;
        $questao->CriadoPor = $this->usuario->id;
        $questao->save();

        foreach ($_questao->respostas as $resposta) {
            $questaoResposta = Model_QuestaoResposta::forge();
            $questaoResposta->Questao = $questao->id;
            $questaoResposta->Letra = $resposta->letra;
            $questaoResposta->Selecionada = $resposta->selecionada ? 'S' : 'N';
            $questaoResposta->Conteudo = $resposta->conteudo;
            $questaoResposta->save();
        }

        echo 'ok';
    }

    /**
     * Modifica um registro com a identificação fornecida.
     */
    public function put_index()
    {
        $id = Input::put('id');

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $questao = Model_Questao::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($questao)) {
                $questao->Assunto = Input::post('assunto');
                $questao->Tipo = Input::post('tipo');
                $questao->Conteudo = Input::post('conteudo');
                $questao->Resposta = Input::post('respostaCerta');
                $questao->Explicacao = (Input::post('explicacao') === 'true') ? "Input::post('explicacao')" : "";
                $questao->save();

                echo 'ok';
            } else {
                echo 'nok';
            }
        } else {
            echo 'nok';
        }
    }

    /**
     * Remove o registro com a identificação fornecida.
     * @return array
     */
    public function delete_index()
    {
        $id = Input::delete('id', -1);
        $removido = false;

        // em tese o registro é válido
        if ($id > 0) {
            // procura o registro por sua identificação
            $questao = Model_Questao::find($id);

            // de fato o registro é válido pois não é nulo
            if (!is_null($questao)) {
                // remove o registro/objeto
                $questao->delete();

                // em tese o registro foi removido, vamos procurar por ele?
                $questao = Model_Questao::find($id);
                // o registro não foi encontrado (!!!)
                if (is_null($questao)) {
                    // é informado que ele foi removido (!!!)
                    $removido = true;
                }
            }
        }

        // o registro não foi removido ou não pode ser removido
        if (!$removido) {
            // não existe a saída em json do método DELETE
            echo 'nok';
        } // o registro foi removido
        else {
            // não existe a saída em json do método DELETE
            echo 'ok';
        }
    }

    /**
     * Remove o registro com a identificação fornecida.
     * @return array
     */
    public function get_questao()
    {
        // em tese o registro é válido
        $id = Uri::segment(5);
        $liberar = (Uri::segment(6) === 'sim');

        // primeiro verifica se em tese é uma identificação
        if ($id > 0) {
            // procura o registro por sua identificação
            $questao = Model_Questao::find($id);
            $questao->save();
            echo 'ok';
        } else {
            echo 'nok';
        }
    }

    /**
     * Processa os registros obtidos.
     * @return array
     */
    private function processarRegistros($registros)
    {
        $retorno = array();

        foreach ($registros as $chave => $registro) {
            $rotulo = '<a href="javascript:void(0);" onclick="javascript:remover();" title="Remover \'' . $registro->Conteudo . '\'"><i class="fam-delete" style="float:right;"></i></a>';
            /*switch ($registro->Situacao) {
                case 'N':
                    $rotulo = '<a href="javascript:void(0);" onclick="javascript:liberar(true);" title="Liberar"><i class="situacao fam-lock"></i></a> ';
                    break;

                case 'L':
                    $rotulo .= '<a href="javascript:void(0);" onclick="javascript:liberar(false);" title="Tirar liberação"><i class="situacao fam-lock-open"></i></a> ';
                    break;

                case 'F':
                    $rotulo = '<i class="situacao fam-stop" title="Finalizado"></i> ';
                    break;
            }*/

            $rotulo .= $registro->Conteudo;
            $valor = $registro->to_array();

            // formata os campos de data para o formato brasileiro
            //$valor['Cadastro'] = $this->formatarDataExibicao($valor['Cadastro']);
            //$valor['Inicio'] = $this->formatarDataExibicao($valor['Inicio']);
            //$valor['Fim'] = $this->formatarDataExibicao($valor['Fim']);

            $retorno[] = array(
                'id' => $registro->id,
                'rotulo' => $rotulo,
                'valor' => $valor
            );

            // liberação da memória
            unset($registros[$chave]);
        }

        // liberação da memória
        unset($registros);

        // retorno do processamento dos registros
        return $retorno;
    }

    /**
     * Obtém uma lista de todos os registros das questẽs por simulado (para janela de vinculação das questões).
     * @return array
     */
    public function get_questoes_simulado()
    {
        $retorno = array();

        // obtém o simulado
        $simulado = Uri::segment(5);

        // obtém as questões
        $query = DB::query('SELECT Questao FROM SimuladoQuestao WHERE (Simulado = ' . $simulado . ')');

        $result = $query->execute();

        while (!is_null($result->current())) {
            $record = (object)$result->current();

            $retorno[] = (int) $record->Questao;

            $result->next();
        }

        echo json_encode($retorno);
        exit();
    }
}
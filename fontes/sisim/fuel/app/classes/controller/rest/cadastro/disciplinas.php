<?php

/**
 * Class Controller_Rest_Cadastro_Assuntos
 *
 * Classe que cuida do CRUD do cadastro dos assuntos.
 */
class Controller_Rest_Cadastro_Disciplinas extends Controller_Rest
{

    private $usuario;

    /**
     * Construtor da classe.
     */
    function __construct(\Request $request) // nesse caso o construtor recebe a requisição tal qual o construtor de seu pai
    {
        // chama o construtor da classe pai
        parent::__construct($request);

        // obtém o usuário da sessão
        $usuario = \Session::get('usuario');

        // obtém a pessoa
        $pessoas = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        foreach ($pessoas as $value) {
            $this->usuario = $value;
        }
    }

    /**
     * Obtém uma lista de todos os registros de disciplinas.
     * @return array
     */
    public function get_index()
    {
        $retorno = array();

        if ($this->usuario->Nivel === 'Professor') {
            // obtém o usuário da sessão
            $usuario = \Session::get('usuario');

            foreach ($usuario->area as $area) {
                $retorno[] = array(
                    'id' => $area->id,
                    'rotulo' => $area->nome,
                    'valor' => $area->id
                );
            }
        } elseif ($this->usuario->Nivel === 'Coordenador') {
            $soap = Request::forge(\Config::get('wsdl'), 'soap');

            $soap->set_function('disciplinas');
            $soap->execute();

            $response = $soap->response();

            foreach ($response->body->return as $area) {
                $retorno[] = array(
                    'id' => $area->id,
                    'rotulo' => $area->nome,
                    'valor' => $area->id
                );
            }
        }

        // retorna os registros encontrados e processados
        return $retorno;
    }
}
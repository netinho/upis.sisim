<?php

/**
 * Class Controller_Rest_Cadastro_Assuntos
 *
 * Classe que cuida do CRUD do cadastro dos assuntos.
 */
class Controller_Rest_Cadastro_Turmas extends Controller_Rest
{

    private $usuario;

    /**
     * Construtor da classe.
     */
    function __construct(\Request $request) // nesse caso o construtor recebe a requisição tal qual o construtor de seu pai
    {
        // chama o construtor da classe pai
        parent::__construct($request);

        // obtém o usuário da sessão
        $usuario = \Session::get('usuario');

        // obtém a pessoa
        $pessoas = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        foreach ($pessoas as $value) {
            $this->usuario = $value;
        }
    }

    /**
     * Obtém uma lista de todos os registros de disciplinas.
     * @return array
     */
    public function get_index()
    {
        $retorno = array();

        if (($this->usuario->Nivel === 'Professor') || ($this->usuario->Nivel === 'Coordenador')) {
            $soap = Request::forge(\Config::get('wsdl'), 'soap');

            $soap->set_function('turmas');
            $soap->execute();

            $response = $soap->response();

            foreach ($response->body->return as $area) {
                $retorno[] = array(
                    'id' => $area->id,
                    'rotulo' => $area->nome,
                    'valor' => $area->id
                );
            }
        }

        // retorna os registros encontrados e processados
        return $retorno;
    }

    /**
     * Obtém uma lista de todos os registros de simulados.
     * @return array
     */
    public function post_simulado()
    {
        $retorno = array();

        if (($this->usuario->Nivel === 'Professor') || ($this->usuario->Nivel === 'Coordenador')) {

            $id = Input::post('id');

            $vinculados = Model_SimuladoTurma::find('all', array(
                'where' => array(
                    array('Simulado', $id),
                ),
            ));

            foreach ($vinculados as $vinculado) {
                $retorno[] = $vinculado->Turma;
            }
        }

        // retorna os registros encontrados e processados
        return $retorno;
    }

    function get_relatorio_simulado()
    {
        // obtém o simulado selecionado
        $selecionado = Uri::segment(6);

        $retorno = array(
            'aaData' => array()
        );

        $query = DB::query('
        SELECT
          SHA1(t.id) AS id,
          t.Nome,
          ROUND(IFNULL(SUM(als.Duracao)/COUNT(als.id), 0), 2) AS TempoMedio,
          ROUND(IFNULL(SUM((als.Pontuacao * 100)/(SELECT si.Pontuacao FROM Simulado si WHERE (si.id = als.Simulado))), 0), 2) AS DesempenhoMedio
        FROM
          SimuladoTurma st
          INNER JOIN Turma t ON
            (t.id = st.Turma)
          LEFT JOIN AlunoTurma alt ON
            (alt.Turma = t.id)
          LEFT JOIN AlunoSimulado als ON
            (als.Simulado = st.Simulado) AND
            (als.AlunoTurma = alt.id)
        WHERE
          (SHA1(st.Simulado) = "' . $selecionado . '")');
        $result = $query->execute();

        while (!is_null($result->current())) {
            $retorno['aaData'][] = (object)$result->current();

            $result->next();
        }

        return $retorno;
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: francisco
 * Date: 5/30/13
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
class Controller_Acesso extends Controller
{
    function action_acessar()
    {
        // verifica se o usuário está acessando
        if (Auth::check()) {
            // caso esteja acessando, redireciona ele
            Response::redirect('inicio/index');
        }

        $val = Validation::forge();
        $val->add_field('usuario', 'Seu usuário', 'required');
        $val->add_field('senha', 'Sua senha', 'required|min_length[3]|max_length[20]');
        if ($val->run()) {
            $auth = Auth::instance();
            if ($auth->login($val->validated('usuario'), $val->validated('senha'))) {
                Session::set_flash('notice', 'FLASH: logged in');
                Response::redirect('');
            } else {
                $data['errors'] = 'Usuário/senha inválidos. Tente novamente.';
            }
        } else {
            if ($_POST) {
                $data['errors'] = 'Usuário/senha inválidos. Tente novamente.';
            } else {
                $data['errors'] = false;
            }
        }

        return Response::forge(View::forge('inicio/index', $data));
    }

    function action_sair()
    {
        // sai do sistema
        Auth::logout();

        // redireciona para a raíz
        Response::redirect('');
    }
}
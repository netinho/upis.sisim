<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Relatorio_Turmas extends Controller_Template
{

    public function __constructor()
    {
        $this->template->set_global('breadcrumb', \Breadcrumb::create_links(), false);
    }

    public function before()
    {
        parent::before();

        if (!Auth::check()) {
            Response::redirect(Uri::base() . '/../../');
        }
    }

    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $this->template->title = 'Relatório :: Turmas';
        $this->template->content = ViewModel::forge('relatorio/turmas/index');

        // obtém o simulado selecionado
        $selecionado = Uri::segment(3);
        $this->template->content->set('selecionado', $selecionado);

        // obtém os simulados
        $query = DB::query('SELECT sha1(id) AS id, Titulo FROM Simulado ORDER BY Titulo ASC');
        $this->template->content->set('simulados', $query->execute()->as_array());

        // obtém o resumo
        if ((!is_null($selecionado)) && (!empty($selecionado))) {
            $query = DB::query('
            SELECT
              s.id,
              CONCAT(DATE_FORMAT(s.Inicio, "%d/%m/%Y %H:%i"), " à ", DATE_FORMAT(s.Fim, "%d/%m/%Y %H:%i")) AS Inicio,
              s.Duracao,
              (SELECT COUNT(id) FROM SimuladoQuestao WHERE (Simulado = s.id)) AS Questoes,
              s.Pontuacao,
              ROUND(IFNULL(SUM(als.Duracao)/COUNT(als.id), 0), 2) AS TempoMedio,
              ROUND(IFNULL(SUM((als.Pontuacao * 100)/(SELECT si.Pontuacao FROM Simulado si WHERE (si.id = als.Simulado))), 0), 2) AS DesempenhoMedio
            FROM
              Simulado s
              LEFT JOIN AlunoSimulado als ON
                (als.Simulado = s.id)
            WHERE
              (SHA1(s.id) = "' . $selecionado . '")');
            $this->template->content->set('resumo', $query->execute()->as_array()[0]);
        }
    }
}
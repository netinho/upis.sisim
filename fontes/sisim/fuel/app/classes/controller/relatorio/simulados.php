<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Relatorio_Simulados extends Controller_Template
{

    public function __constructor()
    {
        $this->template->set_global('breadcrumb', \Breadcrumb::create_links(), false);
    }

    public function before()
    {
        parent::before();

        if (!Auth::check()) {
            Response::redirect(Uri::base() . '/../../');
        }
    }

    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $this->template->title = 'Relatório :: Simulados';
        $this->template->content = ViewModel::forge('relatorio/simulados/index');

        // cálculo da média total
        $query = DB::query('
        SELECT
          ROUND(IFNULL(SUM(als.Duracao)/COUNT(als.id), 0), 2) AS TempoMedio,
          ROUND(IFNULL(SUM((als.Pontuacao * 100)/(SELECT si.Pontuacao FROM Simulado si WHERE (si.id = als.Simulado))), 0), 2) AS DesempenhoMedio
        FROM
          AlunoSimulado als');

        $this->template->content->set('mediaTotal', $query->execute()->as_array()[0]);
    }
}
<?php
/**
 * Fuel
 *
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * Driver de autenticação básica usando webservice.
 *
 * @package     Fuel
 * @subpackage  Auth
 */
class Auth_Login_Wsauth extends \Auth_Login_Driver
{

    public static function _init()
    {
        \Config::load('wsauth', true, true, true);
    }

    /**
     * @var  Database_Result  quando o acesso é permitido
     */
    protected $usuario = null;

    /**
     * @var  array  valor para o acesso do convidado
     */
    protected static $acesso_convidado = array(
        'id' => 0,
        'nome' => 'Convidado',
        'grupo' => '0',
        'hashacesso' => false,
        'usuario' => false
    );

    /**
     * @var  array  Classe de configuração do WsAuth
     */
    protected $config = array(
        'drivers' => array('group' => array('Wsgroup')),
        'additional_fields' => array('perfil'),
    );

    /**
     * Verifica o acesso.
     *
     * @return  bool
     */
    protected function perform_check()
    {
        $usuario = \Session::get('usuario');
        $hashacesso = \Session::get('hashacesso');

        // verifica primeiro se há o usuário e o hash do acesso
        if (!empty($usuario) and !empty($hashacesso)) {
            $this->usuario = $usuario;
            return true;
        }

        // não existe acesso válido quando chega aqui, certifica-se que a sessão esteja vazia e opcionalmente define o acesso de convidado
        $this->usuario = \Config::get('wsauth.guest_login', true) ? static::$acesso_convidado : false;
        \Session::delete('usuario');
        \Session::delete('hashacesso');

        return false;
    }

    /**
     * Verifica se o usuário e a senha existem.
     *
     * @return  bool
     */
    public function validate_user($usuario = '', $senha = '')
    {
        $usuario = trim($usuario) ? : trim(\Input::post(\Config::get('wsauth.username_post_key', 'username')));
        $senha = trim($senha) ? : trim(\Input::post(\Config::get('wsauth.password_post_key', 'password')));

        if (empty($usuario) or empty($senha)) {
            return false;
        }

        $soap = Request::forge(\Config::get('wsauth.wsdl'), 'soap');

        $soap->set_function('acessar');
        $soap->set_params(array(
            'acessar' => array(
                'usuario' => $usuario,
                'senha' => $senha
            )));

        $soap->execute();

        $response = $soap->response();

        $usuario = $response->body->return;

        // verifica se a pessoa já existe
        $pessoa = Model_Pessoa::find('all', array(
            'where' => array(
                array('usuario', $usuario->usuario),
            ),
        ));

        // caso a pessoa não exista
        if (count($pessoa) === 0) {
            // cadastra a pessoa
            $pessoa = Model_Pessoa::forge();
            // a data do cadastro deve ser do sistema
            $pessoa->Cadastro = date('Y-m-d H:i:s');
            $pessoa->Usuario = $usuario->usuario;
            $pessoa->Nome = $usuario->nome;
            $pessoa->Nivel = $usuario->nivel;
            $pessoa->save();
        }

        if ($usuario->autorizado) {
            return $usuario;
        } else {
            return false;
        }
    }

    /**
     * Acesso do usuário.
     *
     * @param   string
     * @param   string
     * @return  bool
     */
    public function login($usuario = '', $senha = '')
    {
        if (!($this->usuario = $this->validate_user($usuario, $senha))) {
            $this->usuario = \Config::get('wsauth.guest_login', true) ? static::$acesso_convidado : false;
            \Session::delete('usuario');
            \Session::delete('hashacesso');
            return false;
        }

        // registrar de modo que Auth::logout() pode ser chamado
        Auth::_register_verified($this);

        \Session::set('usuario', $this->usuario);
        \Session::set('hashacesso', $this->create_login_hash());
        \Session::instance()->rotate();
        return true;
    }

    /**
     * Forca o acesso do usuário.
     *
     * @param   string Identificação do usuário
     * @return  bool
     */
    public function force_login($id = '')
    {
        if (empty($id)) {
            return false;
        }

        $this->usuario = \DB::select_array(\Config::get('wsauth.table_columns', array('*')))
            ->where_open()
            ->where('id', '=', $id)
            ->where_close()
            ->from(\Config::get('wsauth.table_name'))
            ->execute(\Config::get('wsauth.db_connection'))
            ->current();

        if ($this->usuario == false) {
            $this->usuario = \Config::get('wsauth.guest_login', true) ? static::$acesso_convidado : false;
            \Session::delete('usuario');
            \Session::delete('hashacesso');
            return false;
        }

        \Session::set('usuario', $this->usuario->usuario);
        \Session::set('hashacesso', $this->create_login_hash());
        return true;
    }

    /**
     * Saída do usuário.
     *
     * @return  bool
     */
    public function logout()
    {
        $this->usuario = \Config::get('wsauth.guest_login', true) ? static::$acesso_convidado : false;
        \Session::delete('usuario');
        \Session::delete('hashacesso');
        return true;
    }

    /**
     * TODO
     * Create new user
     *
     * @param   string
     * @param   string
     * @param   string  must contain valid email address
     * @param   int     group id
     * @param   Array
     * @return  bool
     */
    public function create_user($email, $password, $group = 1, Array $profile_fields = array())
    {
        throw new Exception('Not implemented.');
    }

    /**
     * TODO
     * Update a user's properties
     * Note: Username cannot be updated, to update password the old password must be passed as old_password
     *
     * @param   Array  properties to be updated including profile fields
     * @param   string
     * @return  bool
     */
    public function update_user($values, $id = null)
    {
        throw new Exception('Not implemented.');
    }

    /**
     * TODO
     * Change a user's password
     *
     * @param   string
     * @param   string
     * @param   string  username or null for current user
     * @return  bool
     */
    public function change_password($old_password, $new_password, $id = null)
    {
        throw new Exception('Not implemented.');
    }

    /**
     * TODO
     * Gera uma nova senha randômica, define-a para o usuário fornecido e retorna a nova senha.
     * Para ser usado para uma senha de usuário esquecida, pode ser enviado por email posteriormente.
     *
     * @param   string $id Identificação do Usuário
     * @return  string
     */
    public function reset_password($id)
    {
        throw new Exception('Not implemented.');
    }

    /**
     * TODO
     * Remove um usuário fornecido.
     *
     * @param   string $id Identificação do Usuário
     * @return  bool
     */
    public function delete_user($id)
    {
        throw new Exception('Not implemented.');
    }

    /**
     * TODO
     * Creates a temporary hash that will validate the current login
     *
     * @return  string
     */
    public function create_login_hash()
    {
        if (empty($this->usuario)) {
            throw new \SimpleUserUpdateException('O usuário não está acessando, não pôde criar um hash do acesso.', 10);
        }

        $ultimo_acesso = \Date::forge()->get_timestamp();
        $hash_acesso = sha1(\Config::get('wsauth.login_hash_salt') . $this->usuario->nome . $ultimo_acesso);

        /*
        \DB::update(\Config::get('wsauth.table_name'))
            ->set(array('UltimoAcesso' => $ultimo_acesso, 'HashAcesso' => $hash_acesso))
            ->where('id', '=', $this->usuario['id'])
            ->execute(\Config::get('wsauth.db_connection'));
        */

        //$this->usuario['hashacesso'] = $hash_acesso;

        return $hash_acesso;
    }

    /**
     * Obtém a identificação do usuário.
     *
     * @return  Array  containing this driver's ID & the user's ID
     */
    public function get_user_id()
    {
        if (empty($this->usuario)) {
            return false;
        }

        return array($this->id, (int)$this->usuario['id']);
    }

    /**
     * Obtém os grupos do usuário.
     *
     * @return  Array  containing the group driver ID & the user's group ID
     */
    public function get_groups()
    {
        if (empty($this->usuario)) {
            return false;
        }

        switch ($this->usuario->nivel) {
            case 'Aluno':
                $grupo = 0;
                break;
            case 'Professor':
                $grupo = 1;
                break;
            case 'Coordenador':
                $grupo = 50;
                break;
            case 'Admin':
                $grupo = 100;
                break;
            default:
                $grupo = -1;
                break;
        }

        return array(array('Wsgroup', $grupo));
    }

    /**
     * Obtém dados do usuário.
     *
     * @param  string $field Nome do campo para retornar
     * @param  mixed $default Valor para retornar se o campo requisitado não existir
     *
     * @return  mixed
     */
    public function get($field, $default = null)
    {
        if (isset($this->usuario[$field])) {
            return $this->usuario[$field];
        } elseif (isset($this->usuario['perfil'])) {
            return $this->get_profile_fields($field, $default);
        }

        return $default;
    }

    /**
     * Obtém o usuário.
     *
     * @return  string
     */
    public function get_username()
    {
        return $this->get('username', false);
    }

    /**
     * Obtém o e-mail do usuário.
     *
     * @return  string
     */
    public function get_email()
    {
        return null;
    }

    /**
     * Obtém o nome do usuário para apresentar.
     *
     * @return  string
     */
    public function get_screen_name()
    {
        if (empty($this->usuario)) {
            return false;
        }

        return $this->usuario->nome;
    }

    /**
     * TODO
     * Get the user's profile fields
     *
     * @return  Array
     */
    public function get_profile_fields($field = null, $default = null)
    {
        if (empty($this->usuario)) {
            return false;
        }

        if (isset($this->usuario['perfil'])) {
            is_array($this->usuario['perfil']) or $this->usuario['perfil'] = @unserialize($this->usuario['perfil']);
        } else {
            $this->usuario['perfil'] = array();
        }

        return is_null($field) ? $this->usuario['perfil'] : \Arr::get($this->usuario['perfil'], $field, $default);
    }

    /**
     * TODO
     * Extension of base driver method to default to user group instead of user id
     */
    public function has_access($condition, $driver = null, $user = null)
    {
        if (is_null($user)) {
            $groups = $this->get_groups();
            $user = reset($groups);
        }
        return parent::has_access($condition, $driver, $user);
    }

    /**
     * Extension of base driver because this supports a guest login when switched on
     */
    public function guest_login()
    {
        return \Config::get('wsauth.guest_login', true);
    }
}

// end of file wsauth.php